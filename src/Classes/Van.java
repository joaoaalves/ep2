/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author joao
 */
public class Van extends Veiculo{

    public Van() {
        setName("Van");
        
        setMaxVel(80);
            
        setCargaMax(3500);
        
        setComb("Diesel");
    }
        
}
