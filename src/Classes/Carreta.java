/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

/**
 *
 * @author joao
 */
public class Carreta extends Veiculo {

    public Carreta() {
        setName("Carreta");
        
        setMaxVel(60);
           
        setCargaMax(30000);

        setComb("Diesel");
    }
   
}
