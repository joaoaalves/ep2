/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author joao
 */
public class Veiculo implements Serializable {
    private String name; //Nome do veiculo
    
    private Integer code; //codigo de cada veículo
    
    private String comb; //tipo de combustivel 
    
    private float rend; //rendimento
    
    private float perda_rend;// perda de rendimento por kg
    
    private int cargamax; //carga maxima
    
    private int carga; //carga atual
    
    private int maxvel; //velocidade maxima
    
    private int numviagem; //numero de viagens feitas
    
    private boolean viajando; //seta viagem ou disponibilidade

    public boolean isViajando() {
        return viajando;
    }

    public void setViajando(boolean viajando) {
        this.viajando = viajando;
    }
    
    public float calculaCustoBeneficio(int distancia){
        if(this.getCarga() > this.getCargaMax())
            return -1;
        
        else
            return this.getCusto(distancia) * ((float)distancia / (float)this.getMaxVel()) ;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    public String getComb() {
        return comb;
    }
    

    public void setComb(String comb) {
        this.comb = comb;
    }
    

    public void setPerda_rend() {
        if(getName() == "Carro")
        {
            if(getComb() == "Gasolina")
                this.perda_rend = (float)0.025;
            
            if(getComb() == "Alcool")
                this.perda_rend = (float)0.0231;
        }
        
        if(getName() == "Moto")
        {
            if(getComb() == "Gasolina")
                this.perda_rend = (float)0.3;
            
            if(getComb() == "Alcool")
                this.perda_rend = (float)0.4;
        }
        
        if(getName() == "Van")
            
            this.perda_rend = (float)0.001;
        
        if(getName() == "Carreta")
            
            this.perda_rend = (float)0.0002;
        
    }
    
    public float getRend() {
        return rend;
    }

    public void setRend() {
        if(getName() == "Carro")
        {
            if(getComb() == "Gasolina")
                this.rend = 14;
            else if(getComb() == "Alcool")
                this.rend = 12;
            else
                this.rend = 0;
        }
        
        if(getName() == "Moto")
        {
            if(getComb() == "Gasolina")
                this.rend = 50;
            
            else if(getComb() == "Alcool")
                this.rend = 42;
            
            else
                this.rend = 0;
        }
        
        if(getName() == "Van")
            this.rend = 10;
        
        if(getName() == "Carreta")
            this.rend = 8;

    }

    public int getCargaMax() {
        return cargamax;
    }

    public void setCargaMax(int cargamax) {
        this.cargamax = cargamax;
    }
    
    public int getCarga() {
        return carga;
    }

    public void setCarga(int carga) {
        this.carga = carga;
    }

    public int getMaxVel() {
        return maxvel;
    }

    public void setMaxVel(int maxvel) {
        this.maxvel = maxvel;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
    
    public float getCusto(Integer distancia){
        float preco_comb = 0;
        

        
            if(comb == "Gasolina")
                preco_comb = (float)4.499;

            if(comb == "Alcool")
                preco_comb = (float)3.499;

            if(comb == "Diesel")
                preco_comb = (float)3.869;

            setRend();
            
            if(rend == 0)
                return 0;

            if(getCarga() > getCargaMax())
                return -1;
         
            else
      
                setPerda_rend();
            
                return  (float) distancia / (float) (rend - ( (float) carga * perda_rend)) * preco_comb;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Veiculo other = (Veiculo) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }



    
}
